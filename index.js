var map = L.map('map').setView([51.505, -0.09], 13);

let accessToken = "pk.eyJ1IjoiZ3JpbmdvLXVhIiwiYSI6ImNsMTk3NHE3eDEzaWszanBmdHFmMHhmcG4ifQ.Lxn100w7cmJLnBGWz2cFvw"

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: "pk.eyJ1IjoiZ3JpbmdvLXVhIiwiYSI6ImNsMTk3NHE3eDEzaWszanBmdHFmMHhmcG4ifQ.Lxn100w7cmJLnBGWz2cFvw",
}).addTo(map);